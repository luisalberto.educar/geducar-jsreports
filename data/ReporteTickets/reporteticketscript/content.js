// server side script fetching remote data and preparing report data source
const axios = require('axios');

// Use the "beforeRender" or "afterRender" hook
// to manipulate and control the report generation
async function beforeRender (req, res) {
    try {
        let cashier_shift_id = req.data.cashier_shift_id || 54;

        const response = await axios.get(
            'https://api.geducar.dev/api/tenant/finance/cashier-shifts/' + cashier_shift_id + '/report', 
            {
                headers: {
                    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmdlZHVjYXIuZGV2L2FwaS9sb2dpbiIsImlhdCI6MTY1OTE0MzgzNSwiZXhwIjoxNjYxNzcxODM1LCJuYmYiOjE2NTkxNDM4MzUsImp0aSI6IkxpU2VTWmU5TlFCd0hDdmoiLCJzdWIiOiIxIiwicHJ2IjoiZDI5YWNmMDc1YTU3MzQ4N2E4NTFlNmEzMTI4MzljMmIxMmZmZDU2ZCIsInN1YmRvbWFpbiI6ImZ1dHVyby5nZWR1Y2FyLmRldiJ9.1L1tnh6vDCQc7ayKb6ZkHQBHbPvOCPmdPBBbh_vRBzg'
                }
            }
        );
        /* const response = await axios.get(
            'http://localhost/api/tenant/finance/cashier-shifts/' + cashier_shift_id + '/report',
            {
                headers: {
                    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FwaVwvbG9naW4iLCJpYXQiOjE2NTkxNDMxNzIsImV4cCI6MTY2MTc3MTE3MiwibmJmIjoxNjU5MTQzMTcyLCJqdGkiOiJzSVNMWFhsa09ZbTJhN01VIiwic3ViIjoxLCJwcnYiOiJkMjlhY2YwNzVhNTczNDg3YTg1MWU2YTMxMjgzOWMyYjEyZmZkNTZkIiwic3ViZG9tYWluIjoiZnV0dXJvLmdlZHVjYXIuZGV2In0.iNBj2bXuILYE3a-K8Yoxg5IlJ1o__7uEGo1ZAVVZNmI'
                }
            }
        ); */
        console.log(response);
        req.data = response.data;
    } catch (error) {
        console.error(error);
    }
}
