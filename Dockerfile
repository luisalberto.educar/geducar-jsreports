FROM jsreport/jsreport:3.6.2

RUN npm i axios --save

COPY ./jsreport.config.json /app/

CMD ["sh", "-c", "node server.js"]
